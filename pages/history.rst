.. title: History of OSS Lugaru
.. slug: history
.. date: 2016-11-20 08:46:06 UTC-05:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text

Lugaru was originally developed by David Rosen of `Wolfire Games`_
and was `open sourced in 2010 <http://blog.wolfire.com/2010/05/Lugaru-goes-open-source>`_.
It was made cross-platform with the help of `Ryan C. Gordon`_.

Various forks were made at that time, and the most interesting developments
were put back together under the OSS Lugaru organization, originally on
`Google Code <https://code.google.com/p/lugaru>`_ and then on
`Bitbucket <https://bitbucket.org/osslugaru/lugaru>`_.

This new repository on `GitLab <https://gitlab.com/osslugaru/lugaru>`_ is run
by the same team, and aims at revitalizing the development effort to clean
things up, ensuring the code base builds and runs fine on all supported
platforms, and easing the packaging of Lugaru in Linux distributions.

Ideally, the updated code base could also be used to update the Lugaru HD
version sold by Wolfire Games once proven better than the current commercial
builds.

.. _Wolfire Games: http://www.wolfire.com
.. _Ryan C. Gordon: http://icculus.org
