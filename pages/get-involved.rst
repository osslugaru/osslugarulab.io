.. title: Getting Involved
.. slug: get-involved
.. date: 2016-11-20 08:58:06 UTC-05:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text

This project is fully community-driven, which means YOU can have an impact
on the future of the game!

We use the following methods of communication:

- GitLab repository (including issues): https://gitlab.com/osslugaru/lugaru
- Mailing list: http://icculus.org/mailman/listinfo/lugaru
- IRC channel: `#Lugaru on Freenode`_

.. _#Lugaru on Freenode: http://webchat.freenode.net/?channels=Lugaru

